"""
A test module to show the discrepancy between python and pylint.
"""

from causality import egg as causality_egg
from causality import chicken as causality_chicken

def main():
    "Instantiate the objects just to keep pylint happy"
    path_infos = ["%s found in %s" % (cls.__name__, cls.__path__) for cls in causality_egg, causality_chicken]
    print '\n'.join(path_infos)
    print 'Imports succesful.'

if __name__ == '__main__':
    main()
