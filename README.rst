This project contains two subdirectories, chicken and egg which will constitute the
``causality`` package using Python's ``pkgutil.extend_path``_ function.

.. _`pkgutil.extend_path`: http://docs.python.org/library/pkgutil.html

* Running ``run_python.sh`` works fine, but
* running ``run_pylint.sh`` doesn't.

This was tested with:

* Linux 2.6.39-bpo.2-amd64 #1 SMP
* Python 2.6.6
* Pylint 0.25.0
* astng 0.23.0, common 0.56.2
